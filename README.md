# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a docker image building instructions to run TDZ™ AceStreamTV with web-interface. More than 500 channels including HD channels
* Ability to run custom acestream ID
* Apple HLS only
* x86_64 only

### How do I get set up? ###

* Clone this repo
* Edit favorites filter: tv/channels.filter
* By default russian audio stream will be selected. You can change «rus» to «eng» in tv/channel.sh:10 if you prefer english audio stream.
* Build image: 
```
docker build -t tv .
```

* Run container: 
```
docker run -d -p 80:80 --name tv tv
```

* Open http://localhost, enter default password «purpurpur»
* Enjoy watching