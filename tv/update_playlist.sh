#!/bin/bash
PWD=$(dirname $0)
OUTPUT=/srv/www/tv/channels.list
FILTER=${PWD}/channels.filter
SOURCES="http://super-pomoyka.us.to/trash/ttv-list/ttv.json http://torrent-telik.com/channels/torrent-tv.json"

TMP=$(mktemp /tmp/XXXXXX)

for s in ${SOURCES}; do
  curl -f -o ${TMP} ${s}
  RC=$?
  if [ ${RC} -eq 0 ]; then break; fi
done
if [ ${RC} -ne 0 ]; then exit 1; fi

TMP2=$(mktemp /tmp/XXXXXX)
#grep -f ${FILTER} ${TMP} > ${TMP2} # apply filter
cat ${TMP} > ${TMP2} # do not apply filter
:>${TMP}

while read channel; do
  NAME=$(echo ${channel} | awk -F '"' '{print $6}')
  LINK=$(echo ${channel} | awk -F '"' '{print $10}')
  CAT=$(echo ${channel} | awk -F '"' '{print $14}')

  if [ -n "${LINK}" ]; then
    NAME=$(echo ${NAME} | sed 's/HD$/<span class="glyphicon glyphicon-hd-video" aria-hidden="true"><\/span>/' | sed '/^HD.*[^S][^D][^)]$/s/$/ <span class="glyphicon glyphicon-hd-video" aria-hidden="true"><\/span>/' | sed 's/(SD)$//')
    if [ -n "${CAT}" ]; then
      echo '<a href="?action=switch&channel='${LINK}'" class="list-group-item">'${NAME}' ('${CAT}')</a>' >> ${TMP}
    else
      echo '<a href="?action=switch&channel='${LINK}'" class="list-group-item">'${NAME}'</a>' >> ${TMP}
    fi
  fi

done < ${TMP2}

mv ${TMP} ${OUTPUT}
grep -f ${FILTER} ${OUTPUT} > /srv/www/tv/favorites.list
