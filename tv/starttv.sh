#!/bin/bash
PWD=$(dirname $0)

service nginx start
service php5-fpm start

sudo -u www-data /srv/tv/update_playlist.sh > /dev/null 2>&1 &
sudo -u www-data /usr/bin/python ${PWD}/aceproxy/acehttp.py > /var/log/aceproxy.log 2>&1
