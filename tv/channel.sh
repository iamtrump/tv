#!/bin/bash -x
ID=$1
NT=0

if [ -n "${ID}" ]; then 
  STREAMS='/srv/www/tv/streams.info'
  ffprobe http://localhost:8000/pid/${ID}/stream.mp4 2>&1 | grep Stream > ${STREAMS}
  if [ $? -eq 1 ]; then echo "Channel «${ID}» could not be played. No seeds?" > /var/log/acestream-to-hls.log; exit 1; fi
  VIDEO=$(cat ${STREAMS} | grep Video | sed 's/.*#\([0-9].[0-9]\)\[.*/\1/' | head -1)
  AUDIO=$(cat ${STREAMS} | grep Audio | grep -i rus | sed 's/.*#\([0-9].[0-9]\)\[.*/\1/' | head -1)
  cat ${STREAMS} | grep 'tv' && CODEC='libx264 -profile:v main -level 4.0' || CODEC=copy
  if [ -z "${AUDIO}" ]; then AUDIO=$(cat ${STREAMS} | grep Audio | sed 's/.*#\([0-9].[0-9]\)\[.*/\1/' | head -1); fi
else
  echo "Stopped manually." > /var/log/acestream-to-hls.log
fi

kill -9 $(ps -ef | grep [f]fmpeg | awk '{print $2}') 2>/dev/null
sleep 2
rm /srv/www/tv/stream*ts

if [ -n "${ID}" ]; then
  ffmpeg -hide_banner -i http://localhost:8000/pid/${ID}/stream.mp4 -f hls -map ${VIDEO}? -map ${AUDIO}? -vcodec ${CODEC} -b:v 2m  -bufsize 2m -acodec aac -ab 96k -start_number 0 -hls_time 5 -hls_list_size 36 -hls_flags delete_segments -threads ${NT} -y /srv/www/tv/stream.m3u8 -loglevel info > /var/log/acestream-to-hls.log 2>&1 &

  # create header
  C=$(grep ${ID} /srv/www/tv/channels.list | sed -e 's/<\/\?a[^<]*>//g' -e 's/(/<small>/g' -e 's/)/<\/small>/g')
  if [ -n "${C}" ]; then echo '<span class="glyphicon glyphicon-play" aria-hidden="true"></span> '$C > /srv/www/tv/current.channel; else echo '<span class="glyphicon glyphicon-play" aria-hidden="true"></span> '${ID} > /srv/www/tv/current.channel; fi
  echo ${ID} > /srv/www/tv/current.id
fi
