FROM debian:jessie
MAINTAINER mikhailmironov@mikhailmironov.ru

RUN echo 'deb http://ftp.debian.org/debian jessie main contrib non-free' > /etc/apt/sources.list && echo 'deb http://ftp.debian.org/debian jessie-updates main contrib non-free' >> /etc/apt/sources.list && echo 'deb http://security.debian.org jessie/updates main contrib non-free' >> /etc/apt/sources.list
RUN apt-get update && apt-get install -y sudo git curl gcc make libx264-dev vlc nginx php5-fpm python-setuptools python-apsw python-gevent python-m2crypto python-psutil

# libx264:
RUN git clone http://git.videolan.org/git/x264.git ~/x264 && cd ~/x264 && ./configure --prefix=/usr/local --disable-asm && make && make install clean

# ffmpeg:
RUN git clone https://git.ffmpeg.org/ffmpeg.git ~/ffmpeg && cd ~/ffmpeg && ./configure --prefix=/usr/local --disable-asm --enable-gpl --enable-libx264 && make && make install clean

# folders and logfiles:
RUN mkdir -p /srv/tv && mkdir -p /srv/www/tv && usermod -d /srv/www www-data
RUN touch /var/log/aceproxy.log /var/log/acestreamengine.log /var/log/acestream-to-hls.log /var/log/vlc.log && chown www-data:www-data /var/log/aceproxy.log /var/log/acestreamengine.log /var/log/acestream-to-hls.log /var/log/vlc.log

# nginx:
RUN rm /etc/nginx/sites-enabled/default
COPY nginx/tv /etc/nginx/sites-available/
RUN ln -s /etc/nginx/sites-available/tv /etc/nginx/sites-enabled/tv
COPY www/index.php www/favicon.jpg /srv/www/tv/

# acestreamengine:
RUN curl -O http://dl.acestream.org/debian/7/acestream_3.0.5.1_debian_7.4_x86_64.tar.gz && tar -xzf acestream_3.0.5.1_debian_7.4_x86_64.tar.gz
RUN mkdir /usr/share/acestream && mv acestream_3.0.5.1_debian_7.4_x86_64/acestreamengine /usr/bin/ && mv acestream_3.0.5.1_debian_7.4_x86_64/acestream.conf /usr/share/acestream/ && mv acestream_3.0.5.1_debian_7.4_x86_64/data /usr/share/acestream/data && mv acestream_3.0.5.1_debian_7.4_x86_64/lib /usr/share/acestream/lib && rm -r acestream_3.0.5.1_debian_7.4_x86_64

# aceproxy:
RUN git clone https://github.com/ValdikSS/aceproxy.git /srv/tv/aceproxy
COPY aceproxy/aceconfig.py /srv/tv/aceproxy/

# backend:
COPY tv/* /srv/tv/
RUN chmod u+x /srv/tv/update_playlist.sh /srv/tv/channel.sh

# priveledges:
RUN chown -R www-data:www-data /srv/tv && chown -R www-data:www-data /srv/www

# run services
CMD /srv/tv/starttv.sh
