<?php
  $password = 'purpurpur';
  $salt = 'wiohejafhui23rjhadskfjhwk4uhrjksefhui243rfnbjk2b4fuisfbkj24briysfbhbk24';
  $md5 = md5($password.$salt);
  @$action=$_GET['action'];
  //set_time_limit (60);
?>
<head>
  <title>TDZ&trade; AceTV</title>
  <link rel="icon" type="image/png" href="/favicon.jpg" />
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
  <div class="container-fluid">
<?php
  @$my_sid=$_COOKIE["my_sid"];
  if ($my_sid != $md5 and $action != 'login'){
?>
    <div class="page-header">
      <h1><span class="glyphicon glyphicon-picture"></span> TDZ&trade; AceTV</h1>
    </div>
    <form action="?action=login" method="post">
      <div class="input-group input-group-lg" style="width:300px">
        <span class="input-group-addon" id="basic-addon1">TV</span>
        <input type="text" class="form-control" name="user" aria-describedby="sizing-addon2">
      </div>
    </form>
<?php
  }
  else {
    switch ($action){
    case 'login':
      @$user=$_POST['user'];
      if ($user == $password) setcookie('my_sid',$md5,time()+9999999999);
?>
    <script>
      window.location = '/'
    </script>
<?php
      break;
    case 'logout':
      setcookie('my_sid','',time()+9999999999);
?>
    <script>
      window.location = '/'
    </script>
<?php
      break;
    case 'switch':
      @$channel=$_POST['channel'];
      if (!isset($channel)) $channel=$_GET['channel'];
      $channel = preg_replace('/[^\w]/','',$channel); // make safe
      $cmd = '/srv/tv/channel.sh '.$channel;
      $ret = shell_exec($cmd);
?>
    <script>
      window.location = '/'
    </script>
<?php
      break;
    case 'update':
      shell_exec('/srv/tv/update_playlist.sh');
?>
    <script>
      window.location = '/'
    </script>
<?php
      break;
    case 'channels':
?>
      <div class="page-header">
        <h1><span class="glyphicon glyphicon-list"></span> All Channels</h1>
      </div>
      <div class="row">
        <div class="col-md-7">
<?php
      echo '<div class="list-group">';
      include('channels.list');
      echo '</div>';
      echo '</div>';
      echo '</div>';
      break;
    case 'debug':
      $ffmpeg = shell_exec('ps -ef | grep [f]fmpeg');
      $ace = shell_exec('ps -ef | grep [a]cestream');
      $aceproxy = shell_exec('ps -ef | grep [a]ceproxy');
      $vlc = shell_exec('ps -ef | grep [v]lc');
      $ffmpeg_log = shell_exec('tail -10 /var/log/acestream-to-hls.log | sed \'s/$/<br>/g\' | sed \'s/\r/<br>\n/g\' | tail -10');
      $aceproxy_log = shell_exec('tail -10 /var/log/aceproxy.log | sed \'s/$/<br>/g\'');
      $tss = shell_exec('(ls -ltr /srv/www/tv/*.ts | tail -10 && echo && echo Total files: $(ls -l /srv/www/tv/*.ts | wc -l), used: $(du -sh /srv/www/tv | awk \'{print $1}\')) | sed \'s/$/<br>/g\'');
      $streams = shell_exec('cat /srv/www/tv/streams.info | sed \'s/$/<br>/g\'');
?>
        <div class="page-header">
          <h1><span class="glyphicon glyphicon-wrench"></span> Debug</h1>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">ffmpeg process</h3>
          </div>
          <div class="panel-body">
            <?=$ffmpeg?>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">ffmpeg log</h3>
          </div>
          <div class="panel-body">
            <?=$ffmpeg_log?>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">stream info</h3>
          </div>
          <div class="panel-body">
            <?=$streams?>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">aceproxy log</h3>
          </div>
          <div class="panel-body">
            <?=$aceproxy_log?>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">latest fragments</h3>
          </div>
          <div class="panel-body">
            <?=$tss?>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">acestream process</h3>
          </div>
          <div class="panel-body">
            <?=$ace?>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">aceproxy process</h3>
          </div>
          <div class="panel-body">
            <?=$aceproxy?>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">vlc process</h3>
          </div>
          <div class="panel-body">
            <?=$vlc?>
          </div>
        </div>
<?php
      break;
    default:
      shell_exec('ps -ef | grep [f]fmpeg || echo \'<span class="glyphicon glyphicon-stop" aria-hidden="true"></span> Stopped\' > /srv/www/tv/current.channel');
?>
      <div class="page-header">
        <h1><?php include('current.channel'); ?></h1>
      </div>
      <div class="row">
        <div class="col-md-8">
          <video width="100%" autoplay controls>
            <source src="stream.m3u8" type="application/x-mpegURL">
            Your browser does not support the video element.
          </video>
          <p>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-3">
          <form action="/?action=switch" method="post">
          <div class="input-group">
            <span class="input-group-addon" id="basic-addon3">acestream://</span>
            <input name="channel" type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3">
          </div>
          </form>
          <p>
          <div class="listgroup pre-scrollable">
            <?php include('/srv/www/tv/favorites.list'); ?>
          </div>
        </div>
      </div>
<?php
    }
?>
      <div class="row">
        <div class="btn-group col-md-8" role="group" aria-label="...">
          <button class="btn btn-default" onclick="location.href='/'"><span class="glyphicon glyphicon-play"></span> Player</button>
          <button class="btn btn-default" onclick="location.href='?action=channels'"><span class="glyphicon glyphicon-list"></span> All Channels</button>
          <button class="btn btn-default" onclick="location.href='?action=update'"><span class="glyphicon glyphicon-refresh"></span> Update Channels</button>
          <button class="btn btn-default" onclick="location.href='?action=switch&channel=<?php $f=fopen('current.id','r'); $id=fgets($f);fclose($f);echo trim($id);?>'"><span class="glyphicon glyphicon-repeat"></span> Restart Channel</button>
          <button class="btn btn-default" onclick="location.href='?action=switch'"><span class="glyphicon glyphicon-stop"></span> Stop Encoding</button>
          <button class="btn btn-default" onclick="location.href='?action=debug'"><span class="glyphicon glyphicon-wrench"></span> Debug</button>
          <button class="btn btn-default" onclick="location.href='?action=logout'"><span class="glyphicon glyphicon-log-out"></span> Logout</button>
        </div>
      </div>
      <p>
<?php
  }
?>
  </div>
</body>
